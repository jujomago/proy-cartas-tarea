import { Component } from "react";
import Header from "./components/Header";
import Tablero from "./components/Tablero";

import baraja from "./utils/baraja";

const initialState = {
  finJuego: false,
  baraja: baraja(),
  parejaSeleccionada: [], // solo colocamos 2 cartas
  estamosComparando: false,
  numeroDeIntentos: 0,
};

export default class App extends Component {
  state = {
    ...initialState,
  };

  seleccinarCatarta = (carta) => {
    console.log("la info de la carta es:", carta);

    /**reglas de validación
     * si estamos comparando no volteamos otras cartas
     * si la carta fue adivinada no la volvemos a volterar
     * si la carta seleccionada, esta en el arreglo comparador tampoco la volteamos
     */

    if (
      this.state.estamosComparando ||
      carta.fueAdivinada ||
      this.state.parejaSeleccionada.indexOf(carta) > -1
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];
    this.setState({ ...this.state, parejaSeleccionada: parejaSeleccionada });

    if (parejaSeleccionada.length === 2) {
      //creamos una funcion para comparar las 2 cartas
      this.compararPareja(parejaSeleccionada);
    }
  };

  compararPareja = (arregloComparador) => {
    this.setState((prevState) => ({ ...prevState, estamosComparando: true }));

    setTimeout(() => {
      const [primeraCarta, segundaCarta] = arregloComparador;
      let baraja = this.state.baraja;

      if (primeraCarta.icono === segundaCarta.icono) {
        baraja = baraja.map((carta) => {
          if (carta.icono !== primeraCarta.icono) {
            return carta;
          } else {
            return { ...carta, fueAdivinada: true };
          }
        });
      }
      const finJuego = baraja.every(item => item.fueAdivinada);
      this.setState({
        baraja: baraja,
        finJuego,
        parejaSeleccionada: [],
        estamosComparando: false,
        numeroDeIntentos: this.state.numeroDeIntentos + 1,
      },()=>{
        if (finJuego)
         alert(`Ganaste el juego en ${this.state.numeroDeIntentos} intentos`)
      });
      

    }, 1000);
  };

  resetearPartida = () => {
    this.setState({ ...initialState, baraja: baraja() });
  };

  render() {
    const { finJuego, numeroDeIntentos, parejaSeleccionada, baraja } = this.state;
    return (
      <div className="App">
        <Header
          intentos={numeroDeIntentos}
          resetear={this.resetearPartida}
        />
        <Tablero
          baraja={baraja}
          parejaSeleccionada={parejaSeleccionada}
          seleccinarCatarta={this.seleccinarCatarta}
        />
        {finJuego && (<div className="container">
          <h3 className={"alert alert-success text-center"}>Ganaste el juego en {numeroDeIntentos} intentos</h3>
        </div>)
        }
      </div>
    );
  }
}
